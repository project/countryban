The Country Ban module allows the admin to set various countries around the world to either 'read only' or to ban them entirely.
Account access will be completely disabled for those IP addresses originating from countries which are set to read only; however, the site
content may still be viewed. 

Setting a country to a "complete" ban will disable the entire website for those IP addresses originating from the designated country. 

Installation
------------
This module relies upon the IP data kindly maintained by Webhosting.info, which can be found here:

http://ip-to-country.webhosting.info/node/view/6

This ip-to-country.csv file *must* be downloaded, unzipped, and added to the Country Ban module folder *before* installation in order for it
to have access to the data on which it relies.

Other standard module installation applies.

